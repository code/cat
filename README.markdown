cat(1)
======

Very naive implementation of `cat(1)` with no options, for me to learn a bit
more C and the toolchain around it. Supports reading from stdin if there are no
arguments, but that's as clever as it gets.

To build, just do:

    $ make

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under an [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.opensource.org/licenses/MIT
