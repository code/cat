.PHONY: all clean

CC = clang
CFLAGS = -std=c90 -Weverything

all : cat

cat : main.o cfn.o cfp.o
	$(CC) $(CFLAGS) -o cat main.o cfn.o cfp.o

clean :
	rm -f -- *.o cat

