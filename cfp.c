#include "cat.h"

/* Function writes the contents of an opened file descriptor to stdout */
int cfp(FILE *fp, void *buf) {
    size_t br;

    /* Use the buffer to read the file in blocks, writing each block to stdout
     * as we go */
    while ((br = fread(buf, 1, BUFLEN, fp)) > 0) {
        if (fwrite(buf, 1, br, stdout) == 0) {
            perror(__FUNCTION__);
            return -1;
        }
    }

    /* If the last return value for br() was -1, there was an error; 0 is what
     * we expect */
    if (ferror(fp) != 0 || ferror(stdout) != 0) {
        perror(__FUNCTION__);
        return -1;
    }

    /* Force a write of any still-buffered data to stdout */
    if (fflush(stdout) != 0) {
        perror(__FUNCTION__);
        return -1;
    }

    /* Return success, since apparently nothing went wrong before we got here
     * */
    return 0;
}

