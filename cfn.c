#include "cat.h"

/* Function opens and writes the contents of a named file to stdout;
 * effectively a wrapper around cfd() */
int cfn(const char *fn, void *buf) {
    FILE *fp;
    int cfpr;

    /* Open the file to get a read-only file descriptor */
    if ((fp = fopen(fn, "r")) == NULL) {
        perror(__FUNCTION__);
        return -1;
    }

    /* Pass the opened descriptor to cfd() to read it; we keep going even if
     * there are problems, because we need the descriptor closed even if we
     * couldn't read it */
    cfpr = cfp(fp, buf);

    /* Close the descriptor, since we should now be done with it */
    if (fclose(fp) != 0) {
        perror(__FUNCTION__);
        return -1;
    }

    /* Done, return the result of the cfp() call */
    return cfpr;
}

