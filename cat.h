#ifndef CAT_H
#define CAT_H

/* Whole bunch of headers I only sort-of understand that are used for the
 * various system functions in the source files */
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/* How big the buffer is to hold the bytes for the read-write cycle from each
 * file descriptor */
#define BUFLEN (2 << 12)

/* Function prototypes so that I can refer to these functions in main() before
 * I actually define them */
int cfn(const char *fn, void *buf);
int cfp(FILE *fp, void *buf);

#endif

